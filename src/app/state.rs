#[cfg(feature = "mongo")]
use mongodb::options::{ClientOptions, ServerApi, ServerApiVersion};

#[cfg(feature = "mongo")]
use crate::settings::database::mongo::MongoSetting;
#[cfg(feature = "redis")]
use crate::settings::database::redis::RedisSetting;
#[cfg(feature = "http_client")]
use crate::settings::http_client::HttpClientSetting;
use crate::settings::Setting;

pub struct AppState {
    pub config: Setting,
    #[cfg(feature = "redis")]
    pub redis_client: redis::Client,
    #[cfg(feature = "mongo")]
    pub mongo_client: mongodb::Database,
    #[cfg(feature = "http_client")]
    pub http_client: reqwest::Client,
}

#[derive(Default)]
pub struct AppStateBuilder {
    #[cfg(feature = "redis")]
    pub redis_client: Option<redis::Client>,
    #[cfg(feature = "mongo")]
    pub mongo_client: Option<mongodb::Database>,
    #[cfg(feature = "http_client")]
    pub http_client: Option<reqwest::Client>,
}

impl AppStateBuilder {
    #[cfg(feature = "redis")]
    pub fn with_redis(&mut self, config: RedisSetting) -> Result<(), redis::RedisError> {
        let client = redis::Client::open(config.uri)?;
        self.redis_client = Some(client);
        Ok(())
    }

    #[cfg(feature = "mongo")]
    pub async fn with_mongo(&mut self, config: MongoSetting) -> Result<(), mongodb::error::Error> {
        let mut client_options = ClientOptions::parse(config.uri).await?;
        // Set the server_api field of the client_options object to Stable API version 1
        let server_api = ServerApi::builder().version(ServerApiVersion::V1).build();
        client_options.server_api = Some(server_api);

        let client = mongodb::Client::with_options(client_options)?;
        let db = client.database(&config.database.clone());
        self.mongo_client = Some(db);
        Ok(())
    }

    #[cfg(feature = "http_client")]
    pub async fn with_http_client(
        &mut self,
        config: HttpClientSetting,
    ) -> Result<(), reqwest::Error> {
        let mut client_builder = reqwest::Client::builder();

        client_builder = match config.timeout {
            Some(timeout) => client_builder.timeout(timeout),
            None => client_builder,
        };

        self.http_client = Some(client_builder.build()?);
        Ok(())
    }

    pub fn build(&self, config: Setting) -> Result<AppState, ()> {
        #[cfg(feature = "redis")]
        if self.redis_client.is_none() {
            return Err(());
        }

        #[cfg(feature = "mongo")]
        if self.mongo_client.is_none() {
            return Err(());
        }

        #[cfg(feature = "http_client")]
        if self.http_client.is_none() {
            return Err(());
        }

        Ok(AppState {
            config,
            #[cfg(feature = "redis")]
            redis_client: self.redis_client.as_ref().unwrap().clone(),
            #[cfg(feature = "mongo")]
            mongo_client: self.mongo_client.as_ref().unwrap().clone(),
            #[cfg(feature = "http_client")]
            http_client: self.http_client.as_ref().unwrap().clone(),
        })
    }
}
