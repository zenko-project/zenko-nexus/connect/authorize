use axum::async_trait;
use futures::TryStreamExt;
use mongodb::{
    bson::{doc, to_document, Document},
    Collection,
};
use tracing::error;

use crate::{
    entities::Entity,
    repositories::{Record, Repository},
    settings::database::mongo::MongoSetting,
};

pub struct MongoRecord {
    id: String,
}

impl Record for MongoRecord {
    fn id(self) -> String {
        self.id
    }
}

pub struct MongoRepository<E: Entity> {
    pub collection: Collection<E>,
}

#[async_trait]
impl<E: Entity> Repository<E, mongodb::Database> for MongoRepository<E> {
    type Config = MongoSetting;
    type Record = MongoRecord;
    type SearchFilters = Option<Document>;

    fn new(client: mongodb::Database, config: Self::Config) -> Self {
        Self {
            collection: client.collection(config.todo_collection.as_str()),
        }
    }

    async fn get_by_id(&self, id: String) -> Option<E> {
        let filter = doc! { "id": id.clone() };
        match self.collection.find_one(filter, None).await {
            Ok(data) => data,
            Err(err) => {
                error!("Error when getting todo {id}: {err}");
                None
            }
        }
    }

    async fn search(&self, filters: Self::SearchFilters) -> Vec<E> {
        let cursor = match self.collection.find(filters, None).await {
            Ok(cursor) => cursor,
            Err(err) => {
                error!("Error when retriving todos: {err}");
                return vec![];
            }
        };

        match cursor.try_collect().await {
            Ok(todos) => todos,
            Err(err) => {
                error!("Error when parsing todos: {err}");
                vec![]
            }
        }
    }

    async fn create(&self, entity: E) -> Option<Self::Record> {
        match self.collection.insert_one(entity, None).await {
            Ok(result) => Some(MongoRecord {
                id: result.inserted_id.to_string(),
            }),
            Err(err) => {
                error!("Error when creating todo: {err}");
                None
            }
        }
    }

    async fn update(&self, entity: E) -> Option<Self::Record> {
        let doc = match to_document(&entity) {
            Ok(doc) => doc,
            Err(err) => {
                error!("Error when creating todo: {err}");
                return None;
            }
        };
        let id = entity.id();
        let filter = doc! { "id": id.clone() };

        match self
            .collection
            .find_one_and_update(filter, doc.clone(), None)
            .await
        {
            Ok(_) => Some(MongoRecord { id }),
            Err(err) => {
                error!("Error when creating todo: {err}");
                None
            }
        }
    }

    async fn delete(&self, id: String) -> Option<Self::Record> {
        let filter = doc! { "id": id.clone() };

        match self.collection.delete_one(filter, None).await {
            Ok(_) => Some(MongoRecord { id }),
            Err(err) => {
                error!("Error when creating todo: {err}");
                None
            }
        }
    }
}
