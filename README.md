# Zenko Connect - Authorize Endpoint

Authorize endpoint for the OIDC Server Zenko Connect

## Table of Contents

- [Release](#release)
- [Deploy](#deploy)


## Release

Changelog must be updated before each release using the [git-cliff tool](https://github.com/orhun/git-cliff)


## Deploy

Update the config file:
- [ ] The environment variable in the [k8s service file](k8s/service.yaml)

```sh
kube apply -f k8s/service.yaml
```

